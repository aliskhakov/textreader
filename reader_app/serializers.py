# coding: utf-8
from models import Profile, Article, Review
from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    u"""
    Сериализатор пользователя
    """
    email = serializers.SlugRelatedField(source='user', slug_field='email')

    class Meta:
        model = Profile
        fields = ('id', 'fio', 'email', 'userpic')


class UserArticleSerializer(serializers.ModelSerializer):
    u"""
    Сериализатор списка текстовых записей пользователя
    """
    user_id = serializers.SlugRelatedField(source='profile', slug_field='id')

    class Meta:
        model = Article
        fields = ('user_id', 'id', 'title', 'created', 'is_read')


class UserArticleDetailSerializer(serializers.ModelSerializer):
    u"""
    Сериализатор текстовой записи пользователя
    """
    class Meta:
        model = Article
        fields = ('title', 'source')


class ArticleSerializer(serializers.ModelSerializer):
    u"""
    Сериализатор списка текстовых записей
    """
    user = UserSerializer(source='profile')

    class Meta:
        model = Article
        fields = ('id', 'title', 'created', 'is_read', 'user')


class ArticleDetailSerializer(serializers.ModelSerializer):
    u"""
    Сериализатор текстовой записи
    """
    user = UserSerializer(source='profile')

    class Meta:
        model = Article
        fields = ('id', 'title', 'created', 'source', 'is_read', 'user')


class CommentSerializer(serializers.Serializer):
    u"""
    Сериализатор вывода комментариев
    """
    id = serializers.IntegerField()
    comment = serializers.CharField()
    count = serializers.IntegerField()
