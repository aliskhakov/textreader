# coding: utf-8
from django.shortcuts import render_to_response
from django.http import HttpResponse

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from models import Profile, Article, Review
from serializers import UserSerializer, UserArticleSerializer, UserArticleDetailSerializer, ArticleSerializer,\
    ArticleDetailSerializer, CommentSerializer


class NewUser(generics.CreateAPIView):
    pass


class UserList(generics.ListAPIView):
    u"""
    Вью списка пользователей
    """
    model = Profile
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    u"""
    Детальное вью пользователя
    """
    model = Profile
    serializer_class = UserSerializer


class UserArticleList(generics.ListAPIView):
    u"""
    Список тектовых записей пользователя
    """
    model = Article
    serializer_class = UserArticleSerializer

    def get_queryset(self):
        queryset = Article.objects.filter(profile=self.kwargs['pk'])

        if not queryset:
            queryset = []

        return queryset


class NewArticle(generics.CreateAPIView):
    u"""
    Создание новой статьи
    """
    model = Article
    serializer_class = UserArticleDetailSerializer

    def pre_save(self, obj):
        obj.profile = Profile(self.kwargs['pk'])


class EditArticle(generics.UpdateAPIView):
    u"""
    Редактирование статьи
    """
    model = Article
    serializer_class = UserArticleDetailSerializer

    def pre_save(self, obj):
        #obj.profile = Profile(self.kwargs['pk'])
        obj.id = self.kwargs['article_id']


class DeleteArticle(generics.DestroyAPIView):
    u"""
    Удаление статьи
    """
    model = Article

    def destroy(self, request, *args, **kwargs):
        obj = Article(self.kwargs['article_id'])
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ArticleList(generics.ListAPIView):
    u"""
    Список всех статей
    """
    model = Article
    serializer_class = ArticleSerializer


class ArticleDetail(generics.RetrieveAPIView):
    u"""
    Чтение статьи
    """
    model = Article
    serializer_class = ArticleDetailSerializer


class ArticleRate(APIView):
    u"""
    Рейтинг статьи
    """
    def get(self, request, **kwargs):
        article_id = kwargs.get('pk')
        article = Article.objects.get(id=article_id)
        rate = article.rating.count()

        return Response({'rate': rate})


class ArticleNewRate(APIView):
    u"""
    Инкремент рейтинга статьи
    """
    def post(self, request, **kwargs):
        article_id = kwargs.get('pk')
        article = Article.objects.get(id=article_id)
        profile = Profile.objects.get(user=request.user)
        article.rating.add(profile)

        return Response(status=status.HTTP_201_CREATED)


class ArticleDeleteRate(APIView):
    u"""
    Инкремент рейтинга статьи
    """
    def post(self, request, **kwargs):
        article_id = kwargs.get('pk')
        article = Article.objects.get(id=article_id)
        profile = Profile.objects.get(user=request.user)
        article.rating.remove(profile)

        return Response(status=status.HTTP_204_NO_CONTENT)


class CommentList(generics.ListAPIView):
    u"""
    Список комментариев
    """
    def list(self, request, *args, **kwargs):
        parent_id = request.GET.get('parent_id', None)
        queryset = Review.objects.filter(article_id=kwargs['pk'], parent=parent_id)

        for comment in queryset:
            comment.count = Review.objects.filter(parent_id=comment.id).count()

        serializer = CommentSerializer(queryset, many=True)

        print serializer.data

        return Response(serializer.data)


class NewComment(generics.CreateAPIView):
    u"""
    Новый комментарий
    """
    model = Review


class EditComment(APIView):
    u"""
    Редактирование комментария
    """
    def post(self, request, **kwargs):
        comment_id = kwargs.get('pk')
        review = Review.objects.get(id=comment_id)
        review.comment = request.DATA.get('comment')
        review.save()

        return Response(status=status.HTTP_201_CREATED)


class DeleteComment(APIView):
    u"""
    Удаление комментария
    """
    def post(self, request, **kwargs):
        comment_id = kwargs.get('pk')
        review = Review.objects.get(id=comment_id)
        review.delete()

        return Response(status=status.HTTP_200_OK)