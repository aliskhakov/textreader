from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from views import UserList, UserDetail, UserArticleList, NewArticle, EditArticle, DeleteArticle, ArticleList, \
    ArticleDetail, ArticleRate, ArticleNewRate, ArticleDeleteRate, CommentList, NewComment, EditComment, DeleteComment


urlpatterns = patterns('',
    url(r'^api-token-auth/', 'rest_framework.authtoken.views.obtain_auth_token'),
    url(r'^user$', UserList.as_view(), name='user-list'),
    url(r'^user/(?P<pk>\d+)$', UserDetail.as_view(), name='user-detail'),
    url(r'^user/(?P<pk>\d+)/article$', UserArticleList.as_view(), name='user-article-list'),
    url(r'^user/(?P<pk>\d+)/article/new$', NewArticle.as_view(), name='new-article'),
    url(r'^user/(?P<pk>\d+)/article/(?P<article_id>\d+)/edit$', EditArticle.as_view(), name='edit-article'),
    url(r'^user/(?P<pk>\d+)/article/(?P<article_id>\d+)/delete$', DeleteArticle.as_view(), name='delete-article'),

    url(r'^article$', ArticleList.as_view(), name='article-list'),
    url(r'^article/(?P<pk>\d+)$', ArticleDetail.as_view(), name='article-detail'),
    url(r'^article/(?P<pk>\d+)/rate$', ArticleRate.as_view(), name='article-rate'),
    url(r'^article/(?P<pk>\d+)/rate/new$', ArticleNewRate.as_view(), name='article-new-rate'),
    url(r'^article/(?P<pk>\d+)/rate/delete$', ArticleDeleteRate.as_view(), name='article-delete-rate'),

    url(r'^article/(?P<pk>\d+)/comment$', CommentList.as_view(), name='comment-list'),
    url(r'^article/(?P<pk>\d+)/comment/new$', NewComment.as_view(), name='new-comment'),
    url(r'^article/(?P<article_id>\d+)/comment/(?P<pk>\d+)/edit$', EditComment.as_view(), name='edit-comment'),
    url(r'^article/(?P<article_id>\d+)/comment/(?P<pk>\d+)/delete$', DeleteComment.as_view(), name='delete-comment'),
)

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])
