# coding: utf-8
from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    u"""
        Модель профиля
    """
    user = models.OneToOneField(User, verbose_name=u'Пользователь', related_name='user')
    fio = models.CharField(verbose_name=u'ФИО', max_length=50)
    userpic = models.CharField(verbose_name=u'Аватар', max_length=500)
    friends = models.ManyToManyField('self')

    def __unicode__(self):
        return u'%s' % (self.user,)

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'


class Article(models.Model):
    u"""
        Модель текстовой записи
    """
    profile = models.ForeignKey(Profile, verbose_name=u'Профиль')
    title = models.CharField(verbose_name=u'Название', max_length=300)
    data = models.TextField(verbose_name=u'Текст записи')
    source = models.CharField(verbose_name=u'Источник', max_length=1000)
    progress = models.SmallIntegerField(null=True, verbose_name=u'Прогресс чтения')
    created = models.DateTimeField(auto_now_add=True, null=False)
    is_read = models.BooleanField(verbose_name=u'Флаг новой статьи', default=0)
    rating = models.ManyToManyField(Profile, verbose_name=u'Рейтинг', related_name='rating', default=0)

    def __unicode__(self):
        return u'%s' % (self.title,)

    class Meta:
        verbose_name = u'Запись'
        verbose_name_plural = u'Записи'


class Review(models.Model):
    u"""
        Модель отзыва
    """
    article = models.ForeignKey(Article, verbose_name=u'Запись')
    user = models.ForeignKey(Profile, verbose_name=u'Пользователь')
    comment = models.CharField(verbose_name=u'Источник', max_length=2000)
    created = models.DateTimeField(auto_now_add=True, null=False)
    parent = models.ForeignKey('self', related_name='children', null=True)

    def __unicode__(self):
        return u'%s - %s' % (self.article, self.user,)

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'
